package com.example.heroesapp

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET ("/api/3588353704732992/search/{name}")
    suspend fun getSuperheroes(@Path("name") superheroName: String): Response<SuperHeroDataResponse>

    @GET("/api/3588353704732992/{id}")
    suspend fun getSuperhero(@Path("id") idSuperhero: String): Response<SuperHeroDetailResponse>


}