package com.example.heroesapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.heroesapp.DetailSuperheroActivity.Companion.EXTRA_ID
import com.example.heroesapp.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    // Esto es el viewBinding
    // Con el ya no neceitamos inicializar los componentes buscandolos por id o inicializar variavles
    private lateinit var binding: ActivityMainBinding
    private lateinit var retrofit: Retrofit

    private lateinit var adapter: SuperheroAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        retrofit = getRetrofit()
        // Para acceder a los id
        // binding.searchView
        initUI()
    }

    private fun initUI() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener
        {
            override fun onQueryTextSubmit(query: String?): Boolean { // esta funcion se va llamar cuando se pulse el boton de buscar en el teclado
                searchByName(query.orEmpty())
                return false
            }

            override fun onQueryTextChange(newText: String?) = false // se va llamar cada vez que escribamos en la caja de texto

        })
//        adapter = SuperheroAdapter { navigateToDetail(it) } // PRO
        adapter = SuperheroAdapter { superheroId -> navigateToDetail(superheroId) } // noob
        binding.rvSuperHero.setHasFixedSize(true)
        binding.rvSuperHero.layoutManager = LinearLayoutManager(this)
        binding.rvSuperHero.adapter = adapter
    }

    private fun searchByName(query: String) {
        binding.progressBar.isVisible = true
        // EL IO es para procesos muy largos
        CoroutineScope(Dispatchers.IO).launch {
            //Dentro de la llave to do se realizara en un hilo secundario
            // Con retrofit crea esta interfaz
            val myResponse: Response<SuperHeroDataResponse> = retrofit.create(ApiService::class.java).getSuperheroes(query)
            if (myResponse.isSuccessful) {
                Log.i("mario8a", "Funciona :D")
                val response: SuperHeroDataResponse? =  myResponse.body()
                if (response != null) {
                    Log.i("mario8a", response.toString())
                    // las corrutinas no pueden cear UI
                    //Solucion
                    runOnUiThread {
                        adapter.updateList(response.superHeroes)
                        binding.progressBar.isVisible = false
                    }
                }
            } else {
                Log.i("mario8a", "No funciona :(")
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        // crear objeto retrofit
        return Retrofit
            .Builder()
            .baseUrl("https://superheroapi.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun navigateToDetail(id: String) {
        val intent = Intent(this, DetailSuperheroActivity::class.java)
        intent.putExtra(EXTRA_ID, id)
        startActivity(intent)
    }
}