package com.example.heroesapp

import com.google.gson.annotations.SerializedName

// la etiqueta @serializeName recibe el nombre que llega del json, pero nos permite colocar caulquier nombre de la variale
//data class SuperHeroDataResponse(val response: String) {
//data class SuperHeroDataResponse(@SerializedName("response") val response: String) {
data class SuperHeroDataResponse(
    @SerializedName("response") val response: String,
    @SerializedName("results") val superHeroes: List<SuperHeroItemResponse>
)

data class SuperHeroItemResponse(
    @SerializedName("id") val superHeroId: String,
    @SerializedName("name") val name: String,
    @SerializedName("image") val superheroImage: SuperheroImageResponse
)

data class SuperheroImageResponse(
    @SerializedName("url") val url: String
)